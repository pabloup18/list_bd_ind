package com.example.list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.list.modelo.Produto;

public class cadastro extends AppCompatActivity {

    private int id = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro produto");
        carregarProduto();
    }

    private void carregarProduto(){
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().get("produtoEdicao") != null){
            Produto produto = (Produto) intent.getExtras().get("produtoEdicao");
            EditText etnome = findViewById(R.id.ET_nome);
            EditText etvalor = findViewById(R.id.ET_valor);
            etnome.setText(produto.getNome());
            etvalor.setText(String.valueOf(produto.getValor()));
            id = produto.getId();
        }
    }

    public void onClickVoltar(View v){
        finish();
    }

    public void onClicSalvar(View v){
        EditText etnome = findViewById(R.id.ET_nome);
        EditText etvalor = findViewById(R.id.ET_valor);
        String nome = etnome.getText().toString();
        Float valor = Float.parseFloat(etvalor.getText().toString());
        Produto produto = new Produto(id, nome, valor);
        Intent intent = new Intent();
        ProdutoDAO produtoDAO = new ProdutoDAO(getBaseContext());
        boolean salvou = produtoDAO.salvar(produto);
        if (salvou){
            finish();
        }else{
            Toast.makeText(cadastro.this, "erro ao salvar", Toast.LENGTH_LONG);
        }
        }
    }
